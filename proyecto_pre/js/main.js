import { scrollActiv, setRRSSLinks } from '../../common_projects_utilities/js/pure-branded-helpers';
import { isMobile, isElementInViewport } from '../../common_projects_utilities/js/dom-helpers';
//Necesario para importar los estilos de forma automática en la etiqueta 'style' del html final
import '../css/main.scss';

//Ejecución
let footer = document.getElementById("footerReference");
let sharing = document.getElementById("sharing");
let viewportHeight = window.innerHeight;
let summary = document.getElementsByClassName("summary");
let summaryArr = Array.prototype.slice.call(summary);

let header = document.getElementById("header");
let headerContent = document.getElementById("headerContent");
let claim = document.getElementById("claim");
let claimHeight;
let headerHeight;
let headerIntersection;
let title = document.getElementById("title");
let marginTitle = 50;
let titleIntersection;
let hasScrolled = false;

document.addEventListener("DOMContentLoaded", function(event) {
	setRRSSLinks();
	if ( !isMobile() ) {
		configureHeader();
	}
	initVideos();
});

window.addEventListener('scroll', function(){
	scrollActiv();
	
	let scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);
	if (isMobile()) {
		if (isElementInViewport(footer)) {
			sharing.classList.remove("visible");
		}
	}

	for(let i = 0; i < summaryArr.length;i++) {
		if(isElementInViewport(summaryArr[i])) {
			if(!summaryArr[i].classList.contains('visible')) {
				summaryArr[i].classList.add('visible');
			}
		}
	}
});


/* CONFIGURE HEADER */
function configureHeader() {
	//Set header values
	setHeaderValues();

	//Add header listeners
	addHeaderListeners();

	//Set header height
	setHeaderHeight();

	//Set claim position
	setClaimPosition();

	//Show title
	showHeaderContent();
}

function setHeaderValues() {
	claimHeight = claim.offsetHeight;
	headerHeight = ( viewportHeight * 2 ) + claimHeight;
	headerIntersection = viewportHeight + claimHeight;
	titleIntersection = viewportHeight - title.offsetTop - title.offsetHeight - marginTitle;
}

function addHeaderListeners() {
	window.addEventListener('scroll', function(){
		if ( !hasScrolled ) {
			setHeaderValues();
			setHeaderHeight();
			hasScrolled = true;
		}
		let scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);
		if ( scrollTop > headerIntersection ){
			sharing.classList.add("visible");
			headerContent.style.position = "absolute";
			headerContent.style.top = headerIntersection + "px";
		} else {
			setClaimPosition();
			sharing.classList.remove("visible");
			headerContent.style.position = "fixed";
			headerContent.style.top = 0;
			if ( scrollTop > titleIntersection ){
				setTitlePosition();
			} else {
				title.style.transform = "translate(-50%,0)";
			}
		}
	});
}

function setHeaderHeight() {
	header.style.height = headerHeight + "px";
}

function setClaimPosition() {
	let scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);
	let claimTranslation = claimHeight - scrollTop;
	claim.style.transform = `translate(0,${claimTranslation}px)`;
}

function showHeaderContent() {
	title.classList.add("title--visible");
	claim.classList.add("header__claim--visible");
}

function setTitlePosition() {
	let scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);
	let titleTranslation = titleIntersection - scrollTop;
	title.style.transform = `translate(-50%,${titleTranslation}px)`;
}


/* INIT VIDEOS */
function initVideos() {
	let playerVideo = '', sourceVideo = '';
	//First
	let videoFirst = document.getElementById("firstVideo");
	playerVideo = "https://www.ecestaticos.com/file/d00289e3146ff152efda0817173f673e/1611307869-video_1_sofa_verde.mp4";
	sourceVideo = document.createElement('source');
	sourceVideo.setAttribute('src', playerVideo);
	videoFirst.appendChild(sourceVideo);

	//Second
	let videoSecond = document.getElementById("secondVideo");
	playerVideo = "https://www.ecestaticos.com/file/689c58ed0c52ae7887032dc3a1a0b08a/1611304659-video_2_vestido.mp4";
	sourceVideo = document.createElement('source');
	sourceVideo.setAttribute('src', playerVideo);
	videoSecond.appendChild(sourceVideo);

	//Third
	/*let videoThird = document.getElementById("thirdVideo");
	playerVideo = "https://www.ecestaticos.com/file/18168e867b170343df8fbd0ad326cef8/1611305554-bano.mp4"; //Cambiar
	sourceVideo = document.createElement('source');
	sourceVideo.setAttribute('src', playerVideo);
	videoThird.appendChild(sourceVideo);*/

	//Fourth
	let videoResponsive = document.getElementById("videoResponsive");
	playerVideo = window.innerWidth < 545 ? "https://www.ecestaticos.com/file/3e67289e83b348fb9acccb705156086d/1611308157-bano_movil.mp4" : "https://www.ecestaticos.com/file/18168e867b170343df8fbd0ad326cef8/1611305554-bano.mp4";
	sourceVideo = document.createElement('source');
	sourceVideo.setAttribute('src', playerVideo);
	videoResponsive.appendChild(sourceVideo);

	//Fifth
	/*let videoFifth = document.getElementById("fifthVideo");
	playerVideo = "https://www.ecestaticos.com/file/18168e867b170343df8fbd0ad326cef8/1611305554-bano.mp4"; //Cambiar
	sourceVideo = document.createElement('source');
	sourceVideo.setAttribute('src', playerVideo);
	videoFifth.appendChild(sourceVideo);*/

	//Sixth
	let videoSixth = document.getElementById("sixthVideo");
	playerVideo = "https://www.ecestaticos.com/file/94929f4f1296a182789e4969dd8c9347/1611305729-final-1.mp4";
	sourceVideo = document.createElement('source');
	sourceVideo.setAttribute('src', playerVideo);
	videoSixth.appendChild(sourceVideo);

	//Seventh
	let videoSeventh = document.getElementById("seventhVideo");
	playerVideo = "https://www.ecestaticos.com/file/1f16a0fe13a0092269eaf32cb490b646/1611305729-final-2.mp4";
	sourceVideo = document.createElement('source');
	sourceVideo.setAttribute('src', playerVideo);
	videoSeventh.appendChild(sourceVideo);	
}